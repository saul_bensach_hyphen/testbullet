package com.testbalas.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Saul on 29/09/2015.
 */

public class Personaje {
    private Texture personaje;
    private static final int ANCHO = 64;
    private static final int ALTO = 96;
    private int xInicial;
    private int yInicial;
    private TextureRegion personajeIdle;
    private Animation personajeAndaAbajo;
    private Animation personajeAndaArriba;
    private Animation personajeAndaIzquierda;
    private Animation personajeAndaDerecha;

    Personaje(int x, int y){
        xInicial = x;
        yInicial = y;
        personaje = new Texture("jugador.png");
        TextureRegion regionPj = new TextureRegion(personaje);
        TextureRegion[][] temp = regionPj.split(ANCHO/4,ALTO/4);
        TextureRegion[] framesPersonajeAndandoDerecha = new TextureRegion[4];
        TextureRegion[] framesPersonajeAndandoIzquierda = new TextureRegion[4];
        TextureRegion[] framesPersonajeAndandoAbajo = new TextureRegion[4];
        TextureRegion[] framesPersonajeAndandoArriba = new TextureRegion[4];
        personajeIdle = temp[0][0];
        for(int i = 0; i < 4; i++){
            framesPersonajeAndandoAbajo[i] = temp[0][i];
        }
        personajeAndaAbajo = new Animation(0.15f,framesPersonajeAndandoAbajo);
        for(int i = 0; i < 4; i++){
            framesPersonajeAndandoDerecha[i] = temp[1][i];
        }
        personajeAndaDerecha = new Animation(0.15f, framesPersonajeAndandoDerecha);
        for(int i = 0; i < 4; i++){
            framesPersonajeAndandoIzquierda[i] = temp[2][i];
        }
        personajeAndaIzquierda = new Animation(0.15f,framesPersonajeAndandoIzquierda);
        for(int i = 0; i < 4; i++){
            framesPersonajeAndandoArriba[i] = temp[3][i];
        }
        personajeAndaArriba = new Animation(0.15f, framesPersonajeAndandoArriba);
    }
    public Animation getAnimacion(char direccion){
       if(direccion == 'n'){
           return personajeAndaArriba;
       }
        if(direccion == 's'){
            return personajeAndaAbajo;
        }
        if(direccion == 'e'){
            return personajeAndaDerecha;
        }
        if(direccion == 'o'){
            return personajeAndaIzquierda;
        }
        return null;
    }
    public TextureRegion getTexturaIdle(){
        return personajeIdle;
    }
    public int getyInicial() {
        return yInicial;
    }

    public void setyInicial(int yInicial) {
        this.yInicial = yInicial;
    }

    public int getxInicial() {
        return xInicial;
    }

    public void setxInicial(int xInicial) {
        this.xInicial = xInicial;
    }
}
