package com.testbalas.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.ArrayList;
import java.util.Random;

public class Test extends ApplicationAdapter {

	Personaje p = null;
	Sound sonido;
	Texture textura;
	Random rand;
	Thread hilo;
	Thread hilo2;
	Thread hilo3;
	ArrayList<Bullet> balas;
	SpriteBatch batch;
	TextureRegion frame;
	Sprite personaje;
	private boolean disparar = false;
	private int lastX = 0;
	private int lastY = 0;
	private char mov = ' ';
	private int pjY;
	private int pjX;
	private boolean seMueve = false;
	private float duracion = 0;

	@Override
	public void create () {
		pjX = 100;
		pjY = 100;
		hilo = new Thread(new Runnable() {
			@Override
			public void run() {
				for(;;){
					disparador();
				}
			}
		});
		hilo2 = new Thread(new Runnable() {
			@Override
			public void run() {
				for(;;){
					if(disparar == true){
						balas.add(new Bullet(lastX,lastY,pjX,pjY));
						sonido.play();
						try {
							Thread.sleep(120);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		});
		p = new Personaje(pjX,pjY);
		sonido = Gdx.audio.newSound(Gdx.files.internal("sonidoLaser.wav"));
		textura = new Texture("muffin.png");
		rand = new Random();
		balas = new ArrayList<Bullet>(0);
		batch = new SpriteBatch();
		hilo.start();
		hilo2.start();
		frame = p.getTexturaIdle();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0.7f, 0.7f, 0.7f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		//disparador();
		capturarMovimientoPersonaje();
		for(int i = 0; i < balas.size(); i++){
			//int x = balas.get(i).getX();
			if(balas.get(i).isBorrarBala()){
				balas.remove(i);
			}else{
				balas.get(i).update();
			}
		}
		batch.begin();
		duracion += Gdx.graphics.getDeltaTime();
		for(int i = 0; i < balas.size(); i++){
			Sprite bala = balas.get(i).getSprite();
			if(balas.get(i).isBarraRotada() == false){
				balas.get(i).setBarraRotada(true);
				bala.rotate(balas.get(i).getAngulo());
			}
			bala.setTexture(textura);
			bala.draw(batch);
		}
		if(seMueve){
			frame = p.getAnimacion(mov).getKeyFrame(duracion,true);
		}else{
			frame = p.getTexturaIdle();
		}
		batch.draw(frame,pjX,pjY);
		batch.end();
	}

	private void disparador(){
		for(;;){
			if(Gdx.input.isTouched()){
				disparar = true;
				seMueve = true;
				lastX = Gdx.input.getX();
				lastY = Math.abs(Gdx.input.getY() - Gdx.graphics.getHeight());
			}else{
				disparar = false;
			}
		}
	}
	private void capturarMovimientoPersonaje() {
		if(seMueve){
			if(pjY < lastY){
				mov = 'n';
				pjY++;
			}
			if(pjY > lastY){
				mov = 's';
				pjY--;
			}
			if(pjX < lastX){
				mov = 'e';
				pjX++;
			}
			if(pjX > lastX){
				mov = 'o';
				pjX--;
			}
			if(pjY == lastY && pjX == lastX){
				mov = ' ';
				seMueve = false;
			}
		}
	}

}
