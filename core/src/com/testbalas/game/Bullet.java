package com.testbalas.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

import java.awt.Point;

/**
 * Created by Saul on 30/09/2015.
 */
public class Bullet {

    private Sprite spriteBala;
    private int xInicial;
    private int yInicial;
    private int x;
    private int y;
    private int objetivoX;
    private int objetivoY;
    private int height;
    private int width;
    private float angulo;
    private boolean borrarBala = false;
    private boolean barraRotada;

    Bullet(int x1, int y1, int x2, int y2){
        spriteBala = new Sprite(new Texture("muffin.png"));
        xInicial = x2;
        yInicial = y2;
        x = xInicial;
        y = yInicial;
        objetivoX = x1;
        objetivoY = y1;
        spriteBala.setPosition(xInicial,yInicial);
        angulo = (float) Math.toDegrees(Math.atan2(objetivoY - yInicial, objetivoX - xInicial));
        barraRotada = false;
        height = Gdx.graphics.getHeight();
        width = Gdx.graphics.getWidth();
    }
    public void update(){
        if(y < objetivoY){
            y+=30;
        }
        if(y > objetivoY){
            y-=30;
        }
        if(x < objetivoX){
            x+=30;
        }
        if(x > objetivoX){
            x-=30;
        }
        int distanciay = Math.abs(objetivoY - y);
        int distanciax = Math.abs(objetivoX - x);
        if(distanciax <= 30 && distanciay <= 30){
            borrarBala = true;
        }
        //System.out.println(y +" "+objetivoY);
        spriteBala.setPosition(x,y);
    }
    public void sumarX(int x){
        this.x += x;
    }
    public void sumarY(int y){
        this.y += y;
    }
    public Sprite getSprite(){
        return spriteBala;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public int getObjetivoX() {
        return objetivoX;
    }

    public void setObjetivoX(int objetivoX) {
        this.objetivoX = objetivoX;
    }

    public int getObjetivoY() {
        return objetivoY;
    }

    public void setObjetivoY(int objetivoY) {
        this.objetivoY = objetivoY;
    }
    public boolean isBorrarBala() {
        return borrarBala;
    }
    public float getAngulo() {
        return angulo;
    }

    public boolean isBarraRotada() {
        return barraRotada;
    }

    public void setBarraRotada(boolean barraRotada) {
        this.barraRotada = barraRotada;
    }
}
